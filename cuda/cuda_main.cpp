/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - cuda_main.cpp
 * -------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include "cuda_utils.h"

extern "C" float convolutionGPU(const unsigned int, const unsigned int,
    const color_t, const unsigned char*, const unsigned int,
    const unsigned int, unsigned char*);

int main(int argc, char** argv) {

    // check for correct usage
    if (argc != 6) {

        // wrong command line arguments, print usage message
        printf("\nUsage: %s image width height isRGB steps.\n\n"
        "\timage :\tFilename of the image you wish to apply the filter.\n"
        "\twidth :\tWidth of the image in pixels.\n"
        "\theight:\tHeight of the image in pixels.\n"
        "\tisRGB :\t0 for grey, 1 for rgb.\n"
        "\tsteps :\tTimes you wish to apply the filter to the image.\n\n", argv[0]);

        // return unsuccessfully to the operating system
        return EXIT_FAILURE;
    }

    // read parameters
    const char *filename = argv[1];
    const unsigned int Width = atoi(argv[2]);
    const unsigned int Height = atoi(argv[3]);
    const color_t color = (argv[4][0] == '0') ? GREY : RGB;
    const unsigned int steps = atoi(argv[5]);
    const unsigned int Size = Height * Width * color;

    // the filter
    const unsigned char filter[9] = {1,2,1,2,4,2,1,2,1};

    // create a buffer in the size of the image
    // open the file in order to read
    // read all the input data and place them to the buffer
    // close the file
    unsigned int io_check = 0;
    unsigned char *buf = (unsigned char *) malloc(Size * sizeof(unsigned char));
    if (!buf) { // check if malloc succeed
        printf("Ooops, I run out of memory!\n");
        return EXIT_FAILURE;
    }
    FILE *input_file = fopen(filename, "rb");
    while (io_check < Size * sizeof(unsigned char))
        io_check += fread(buf, 1, Size * sizeof(unsigned char) - io_check,
            input_file);
    fclose(input_file);

    // run the CUDA code, calculate the elapsed time, and print it
    printf("Elapsed time: %.3f sec.\n",
        convolutionGPU(Width, Height, color, filter, steps, Size, buf) / 1000);

    // create a file named "result.raw" and open it to write the output image
    // write all the output data from the buffer
    // close the file
    io_check = 0;
    FILE *output_file = fopen("output.raw", "wb");
    while (io_check < Size * sizeof(unsigned char))
        io_check += fwrite(buf, 1, Size * sizeof(unsigned char) - io_check,
            output_file);
    fclose(output_file);

    // free the allocated memory
    free(buf);

    // everything went well
    // return successfully to the operating system
    return EXIT_SUCCESS;
}
