/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - cuda_utils.h
 * -------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

#ifndef _CUTIL_H_
#define _CUTIL_H_

#include <stdio.h>
#include <cuda.h>

#define CUDA_SAFE_CALL(call) { \
    cudaError err = call; \
    if( cudaSuccess != err) { \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
            __FILE__, __LINE__, cudaGetErrorString( err) ); \
        exit(EXIT_FAILURE); \
} }

#define FRACTION_CEILING(numerator, denominator) \
    ((numerator + denominator - 1) / denominator)

#endif

#define FILTER(v,x,y) (v*filter[-y*3-x+4])

#define COMPAREROUNDS 10

// colors:
// GREY (1 byte  data)
// RGB  (3 bytes data)
typedef enum {GREY = 1, RGB = 3} color_t;
