/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - cuda_utils.cu
 * -------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include "cuda_utils.h"

__global__ void cuda_conv(const unsigned int width, const unsigned int height,
    const color_t color, unsigned char *filter, unsigned int total,
    unsigned char *src, unsigned char *dest) {

    // find current thread x and y
    const unsigned int x = threadIdx.x + blockIdx.x * blockDim.x;
    const unsigned int y = threadIdx.y + blockIdx.y * blockDim.y;

    if (x < width && y < height) {

        unsigned int offset = (x + y * width) * color;

        // if we are on the left edge
        // offset to go left should be 0 as we can't go left
        const unsigned int N = (y == 0)          ? 0 : -color * width;
        const unsigned int S = (y == height - 1) ? 0 : color * width;
        const unsigned int W = (x == 0)          ? 0 : -color;
        const unsigned int E = (x == width - 1)  ? 0 : color;

        // same as above, but calculate each channel separately
        for (int i = 0; i < color; i++)
        {
            // multiply neighbouring pixels by their coefficients and add to result
            // divide result by the sum of all coefficients and write to target
            dest[offset] = (FILTER(src[offset + N + W], 1, 1) \
                + FILTER(src[offset + N], 0, 1) \
                + FILTER(src[offset + N + E], -1, 1) \
                + FILTER(src[offset + W], 1, 0) \
                +  FILTER(src[offset], 0, 0) \
                + FILTER(src[offset + E], -1, 0) \
                + FILTER(src[offset + S + W], 1, -1) \
                + FILTER(src[offset + S], 0, -1) \
                + FILTER(src[offset + S + E], -1, -1)) / total;

                offset++;
        }
    }
}

__global__ void cuda_memcmp(unsigned char *buf1, unsigned char *buf2,
    unsigned int len, char *res) {

    // if we find any difference, write 0 to output and return
    for (unsigned int i = 0; i < len; i++)
        if (buf1[i] != buf2[i]) {
            *res = 0;
            return;
        }

    // otherwise write 1
    *res = 1;
}

extern "C" float convolutionGPU(const unsigned int width, const unsigned int height,
    const color_t color, const unsigned char *filter, const unsigned int steps,
    const unsigned int Size, unsigned char *buf){

    unsigned char *dev_buf1, *dev_buf2;
    unsigned char *dev_filter;

    // allocate needed device buffers
    CUDA_SAFE_CALL(cudaMalloc((void**)&dev_buf1, Size));
    CUDA_SAFE_CALL(cudaMalloc((void**)&dev_buf2, Size));
    CUDA_SAFE_CALL(cudaMalloc((void**)&dev_filter, 9 * sizeof(unsigned char)));

    // copy data to device memory
    CUDA_SAFE_CALL(cudaMemcpy(dev_buf1, buf, Size, cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaMemcpy(dev_filter, filter, 9 * sizeof(unsigned char),
        cudaMemcpyHostToDevice));

    dim3 dimBl(24, 32);
    dim3 dimGr(FRACTION_CEILING(width, 24), FRACTION_CEILING(height, 32));
    unsigned int total = 0;
    for (int i = 0; i < 9; i++)
        total += filter[i];

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    char converge = 0;
    char *dev_converge;
    // allocate one byte on the device as a flag for convergence
    CUDA_SAFE_CALL(cudaMalloc(&dev_converge, 1));
    // start with convergence = 0
    CUDA_SAFE_CALL(cudaMemcpy(dev_converge, &converge, 1, cudaMemcpyHostToDevice));

    // run the convolution function twice, once with buf1 as the source
    // and buf2 as the target and once in reverse
    for (int i = 0; i < steps / 2; i++) {
        cuda_conv<<<dimGr, dimBl>>>(width, height, color, dev_filter, total,
            dev_buf1, dev_buf2);
        cuda_conv<<<dimGr, dimBl>>>(width, height, color, dev_filter, total,
            dev_buf2, dev_buf1);

        if ((i % COMPAREROUNDS) == COMPAREROUNDS - 1) {

            // every 10 loops (20 rounds), compare the two buffers
            cuda_memcmp<<<1, 1>>>(dev_buf1, dev_buf2, Size, dev_converge);
            // copy the flag from the device to the host
            CUDA_SAFE_CALL(cudaMemcpy(&converge, dev_converge, 1,
                cudaMemcpyDeviceToHost));

            // if no change between them, break
            if (converge == 1)
                break;
        }
    }

    // if the last bit is 1,
    // we need to run it one more time and move the result to dev_buf1
    if (converge == 0 && (steps & 1) == 1) {
        cuda_conv<<<dimGr, dimBl>>>(width, height, color, dev_filter, total,
            dev_buf1, dev_buf2);
        unsigned char *tmp = dev_buf1;
        dev_buf1 = dev_buf2;
        dev_buf2 = tmp;
    }

    CUDA_SAFE_CALL(cudaFree(dev_converge));

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);

    CUDA_SAFE_CALL(cudaGetLastError());
    CUDA_SAFE_CALL(cudaThreadSynchronize());

    // copy results back to host memory
    CUDA_SAFE_CALL(cudaMemcpy(buf, dev_buf1, Size, cudaMemcpyDeviceToHost));

    CUDA_SAFE_CALL(cudaFree(dev_buf1));
    CUDA_SAFE_CALL(cudaFree(dev_buf2));
    CUDA_SAFE_CALL(cudaFree(dev_filter));

    return milliseconds;
}
