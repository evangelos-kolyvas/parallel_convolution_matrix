/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - hybrid_main.c
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include "hybrid_utils.h"

int main(int argc, char **argv) {

    // start MPI
    MPI_Init(&argc, &argv);

    // find total number of processes and my rank
    int numprocs, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // check for correct usage
    if (rank == 0 && argc != 6) {

        // wrong command line arguments, print usage message
        printf("\nUsage: %s image width height isRGB steps.\n\n"
        "\timage :\tFilename of the image you wish to apply the filter.\n"
        "\twidth :\tWidth of the image in pixels.\n"
        "\theight:\tHeight of the image in pixels.\n"
        "\tisRGB :\t0 for grey, 1 for rgb.\n"
        "\tsteps :\tTimes you wish to apply the filter to the image.\n\n", argv[0]);

        // terminate MPI
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);

        // return unsuccessfully to the operating system
        return EXIT_FAILURE;
    }

    // read parameters
    const char *filename = argv[1];
    const unsigned int Height = atoi(argv[3]);
    const color_t color = (argv[4][0] == '0') ? GREY : RGB;
    const unsigned int Width = atoi(argv[2]) * color;
    const unsigned int steps = atoi(argv[5]);
    const unsigned int Size = Height * Width;

    // processes per dimension, width, height and size to each process
    const unsigned int procs_per_dimension = sqrt((double)numprocs);
    const unsigned int width = Width / procs_per_dimension;
    const unsigned int height = Height / procs_per_dimension;
    const unsigned int size = height * width;

    // create cartesian communicator. use topology for better mapping.
    // processes that communicate often place them next to each other
    int sides[2] = {procs_per_dimension, procs_per_dimension};
    int periodic[2] = {1, 1};
    MPI_Comm cartesian;
    MPI_Cart_create(MPI_COMM_WORLD, 2, sides, periodic, 1, &cartesian);

    // in case you are the master process,
    // create a buffer in the size of the image
    // open the file in order to read
    // read all the input data and place them to the buffer
    // close the file
    unsigned int io_check = 0;
    unsigned char *buf = NULL;
    if (rank == 0) {
        buf = malloc(Size * sizeof(unsigned char));
        if (!buf) { // check if malloc succeed
            printf("Ooops, I run out of memory! Can't create master buffer!\n");
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
            return EXIT_FAILURE;
        }
        FILE *input_file = fopen(filename, "rb");
        while (io_check < Size * sizeof(unsigned char))
            io_check += fread(buf, 1, Size * sizeof(unsigned char) - io_check,
                input_file);
        fclose(input_file);
    }

    // create local buffers used by each process to filter the image
    // local_buf1: image before applying the filter in each step
    // local_buf2: image after  applying the filter in each step
    // we use a single malloc operation in order to allocate continuous memory
    // we set 2nd pointer buffer in the middle of the 1st buffer
    unsigned char *local_buf1 = malloc(2 * size * sizeof(unsigned char));
    if (!local_buf1) { // check if malloc succeed
        printf("rank %d: Ooops, I run out of memory!\n", rank);
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        return EXIT_FAILURE;
    }
    unsigned char *local_buf2 = &local_buf1[size];

    // set displacements and counts
    // we will use these with MPI_Scatterv
    // in order to scatter blocks of the image to the processes
    int *disps = malloc(size*sizeof(int));
    int *counts = malloc(size*sizeof(int));
    for (int i = 0; i < procs_per_dimension; i++) {
        for (int j = 0; j < procs_per_dimension; j++) {
            disps[i * procs_per_dimension + j] = i * Width * height + j * width;
            counts [i * procs_per_dimension + j] = 1;
        }
    }

    // create datatype resized_block for scattering blocks
    MPI_Datatype block;
    MPI_Type_vector(height, width, Width, MPI_UNSIGNED_CHAR, &block);
    MPI_Type_commit(&block);

    MPI_Datatype resized_block;
    MPI_Type_create_resized(block, 0, sizeof(unsigned char), &resized_block);
    MPI_Type_commit(&resized_block);

    // scatter blocks to processes
    MPI_Scatterv(buf, counts, disps, resized_block, local_buf1,
        size, MPI_UNSIGNED_CHAR, 0, cartesian);

    // process image
    unsigned char *res_buf = convolution(rank, width, height,
        color, steps, cartesian, local_buf1, local_buf2);

    // gather blocks from processes to make the output image
    MPI_Gatherv(res_buf, size, MPI_UNSIGNED_CHAR, buf, counts, disps,
        resized_block, 0, cartesian);

    // in case you are the master process,
    // create a file named "result.raw" and open it to write the output image
    // write all the output data from the buffer
    // close the file
    io_check = 0;
    if (rank == 0)
    {
        FILE *output_file = fopen("output.raw", "wb");
        while (io_check < Size * sizeof(unsigned char))
            io_check += fwrite(buf, 1, Size * sizeof(unsigned char) - io_check,
                output_file);
        fclose(output_file);

        // if you are done, free the master buffer
        free(buf);
    }

    // free defined custom datatype for block and resized_block
    MPI_Type_free(&block);
    MPI_Type_free(&resized_block);

    // all processes free their local buffer
    free(local_buf1);

    // stop MPI
    MPI_Finalize();

    // everything went well
    // return successfully to the operating system
    return EXIT_SUCCESS;
}
