/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - hybrid_utils.c
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include "hybrid_utils.h"

// save the ranks of each process' neighbours in neighbour_ranks[]
void get_ranks(int cur_rank, MPI_Comm cartesian, int neighbour_ranks[]) {

    int coords[2];
    MPI_Cart_coords(cartesian, cur_rank, 2, coords);
    neighbour_ranks[C] = cur_rank;
    coords[1]--;
    coords[0]--;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[NW]);
        coords[1]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[N]);
        coords[1]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[NE]);
        coords[0]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[E]);
        coords[1] -= 2;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[W]);
        coords[0]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[SW]);
        coords[1]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[S]);
        coords[1]++;
    MPI_Cart_rank(cartesian, coords, &neighbour_ranks[SE]);
}

/**************************************************************************
 * core function, image convolution, definition
 * process the image in src_buf, total_steps times,
 * the image is copied between src_buf and dest_buf while being processed
 * returns the buffer the final version of the image is in
 **************************************************************************/
unsigned char *convolution(int rank, const int width, const int height,
    const color_t color, const unsigned int total_steps,
    MPI_Comm cartesian, unsigned char *src_buf, unsigned char *dest_buf) {

    // arrays of send and receive requests
    MPI_Request sendRequests[9], recvRequests[9];

    // 4 corners receiving values
    unsigned char NWc[color], NEc[color], SWc[color], SEc[color];

    // allocate buffers for receiving lines (North and South)
    unsigned char *Nline = malloc(width * sizeof(unsigned char));
    unsigned char *Sline = malloc(width * sizeof(unsigned char));

    // allocate buffers for receiving columns (West and East)
    unsigned char *Wcolumn = malloc(height * color * sizeof(unsigned char));
    unsigned char *Ecolumn = malloc(height * color * sizeof(unsigned char));

    // save the ranks of each process' neighbours in neighbour_ranks[]
    int neighbour_ranks[9];
    get_ranks(rank, cartesian, neighbour_ranks);

    // get ranks of neighbouring processes
    const int rNW = neighbour_ranks[NW];
    const int rN  = neighbour_ranks[N];
    const int rNE = neighbour_ranks[NE];
    const int rW  = neighbour_ranks[W];
    const int rE  = neighbour_ranks[E];
    const int rSW = neighbour_ranks[SW];
    const int rS  = neighbour_ranks[S];
    const int rSE = neighbour_ranks[SE];

    // NE, SW, SE pixels' index
    const int NEp = width - color;
    const int SWp = (height - 1) * width;
    const int SEp = height * width - color;

    // timer and filter
    double timer;
    const float filter[9] = {0.0625, 0.125, 0.0625,
        0.125, 0.25, 0.125, 0.0625, 0.125, 0.0625};

    // we need this for the swap between src and dest buffers
    // at the end of each round
    unsigned char *tmp;

    // declare a custom datatype representing a pixel
    MPI_Datatype pixel;
    MPI_Type_contiguous(color, MPI_UNSIGNED_CHAR, &pixel);
    MPI_Type_commit(&pixel);

    // declare a custom datatype representing a line of pixels
    MPI_Datatype line;
    MPI_Type_contiguous(width, MPI_UNSIGNED_CHAR, &line);
    MPI_Type_commit(&line);

    // declare a custom datatype representing a column of pixels
    MPI_Datatype column;
    MPI_Type_vector(height, color, width, MPI_UNSIGNED_CHAR, &column);
    MPI_Type_commit(&column);

    // synchronize all processes
    MPI_Barrier(cartesian);

    // start counting time
    timer = MPI_Wtime();

    for (int step = 0; step < total_steps; step++) {
        // send needed pixels to other processes
        MPI_Isend(src_buf,       1, pixel,  rNW, NW, cartesian, &sendRequests[NW]);
        MPI_Isend(src_buf,       1, line,   rN,  N,  cartesian, &sendRequests[N]);
        MPI_Isend(&src_buf[NEp], 1, pixel,  rNE, NE, cartesian, &sendRequests[NE]);
        MPI_Isend(src_buf,       1, column, rW,  W,  cartesian, &sendRequests[W]);
        MPI_Isend(&src_buf[NEp], 1, column, rE,  E,  cartesian, &sendRequests[E]);
        MPI_Isend(&src_buf[SWp], 1, pixel,  rSW, SW, cartesian, &sendRequests[SW]);
        MPI_Isend(&src_buf[SWp], 1, line,   rS,  S,  cartesian, &sendRequests[S]);
        MPI_Isend(&src_buf[SEp], 1, pixel,  rSE, SE, cartesian, &sendRequests[SE]);

        sendRequests[C] = MPI_REQUEST_NULL;

        // receive needed pixels from other processes
        MPI_Irecv(&NWc,         1, pixel, rNW, SE, cartesian, &recvRequests[NW]);
        MPI_Irecv(Nline,        1, line,  rN,  S,  cartesian, &recvRequests[N]);
        MPI_Irecv(&NEc,         1, pixel, rNE, SW, cartesian, &recvRequests[NE]);
        MPI_Irecv(Wcolumn, height, pixel, rW,  E,  cartesian, &recvRequests[W]);
        MPI_Irecv(Ecolumn, height, pixel, rE,  W,  cartesian, &recvRequests[E]);
        MPI_Irecv(&SWc,         1, pixel, rSW, NE, cartesian, &recvRequests[SW]);
        MPI_Irecv(Sline,        1, line,  rS,  N,  cartesian, &recvRequests[S]);
        MPI_Irecv(&SEc,         1, pixel, rSE, NW, cartesian, &recvRequests[SE]);

        recvRequests[C] = MPI_REQUEST_NULL;

        // calculate inside pixels
        // if using OpenMP, use THREADSPERCORE threads per core
        // THREADSPERCORE is defined at hybrid_utils.h
        #ifdef _OPENMP
        #pragma omp parallel for num_threads(THREADSPERCORE)
        #endif
        for (int i = 1; i < height - 1; i++)
            for (int l = color; l < width - color; l++)
                // find offset of current pixel in array
                // for inside pixels, just add all 9 pixels
                // multiplied by their coefficients
                // divide by total and write to output
                dest_buf[i * width + l] = ( \
                    ADD_UPPER_ROW(i * width + l, color) \
                    + ADD_MIDDLE_ROW(i * width + l, color) \
                    + ADD_DOWN_ROW(i * width + l, color));

        // wait until we receive all needed pixels for pixels on the edges
        MPI_Waitall(8, recvRequests, MPI_STATUSES_IGNORE);

        // calculate pixels on top edge
        for (int i = color; i < width - color; i++)
            dest_buf[i] = (ADD_MIDDLE_ROW(i, color) \
                + ADD_DOWN_ROW(i, color) \
                + Nline[i - color] * filter[SE] \
                + Nline[i] * filter[S] \
                + Nline[i + color] * filter[SW]);

        // calculate pixels on bottom edge
        for (int i = color; i < width - color; i++)
            dest_buf[(height - 1) * width + i] = ( \
                ADD_UPPER_ROW((height - 1) * width + i, color) \
                + ADD_MIDDLE_ROW((height - 1) * width + i, color) \
                + Sline[i - color] * filter[NE] \
                + Sline[i] * filter[N] \
                + Sline[i + color] * filter[NW]);

        // process corners and left and right edges for each stream
        for (unsigned char stream = 0; stream < color; stream++) {

            // calculate pixels on left edge
            for (int i = 1; i < height - 1; i++)
                dest_buf[i * width + stream] = ( \
                    ADD_MIDDLE_COL(i * width + stream, color) \
                    + ADD_RIGHT_COL(i * width + stream, color) \
                    + Wcolumn[(i - 1) * color + stream] * filter[SE] \
                    + Wcolumn[i * color + stream] * filter[E] \
                    + Wcolumn[(i + 1) * color + stream] * filter[NE]);

            // calculate pixels on right edge
            for (int i = 1; i < height - 1; i++)
                dest_buf[(i + 1) * width - color + stream] = ( \
                    ADD_MIDDLE_COL((i + 1) * width - color + stream, color) \
                    + ADD_LEFT_COL((i + 1) * width - color + stream, color) \
                    + Ecolumn[(i - 1) * color + stream] * filter[SW] \
                    + Ecolumn[i * color + stream] * filter[W] \
                    + Ecolumn[(i + 1) * color + stream] * filter[NW]);

            // calculate corners
            // NW corner
            dest_buf[stream] = (src_buf[stream] * filter[C] \
                + src_buf[stream + color] * filter[W] \
                + src_buf[stream + width] * filter[N] \
                + src_buf[stream + width + color] * filter[NW] \
                + Nline[stream] * filter[S] \
                + Nline[stream + color] * filter[SW] \
                + Wcolumn[stream] * filter[E] \
                + Wcolumn[stream + color] * filter[NE] \
                + NWc[stream] * filter[SE]);

            // NE corner
            dest_buf[width + stream - color] = ( \
                src_buf[width + stream - color] * filter[C] \
                + src_buf[width + stream - 2 * color] * filter[E] \
                + src_buf[2 * width + stream - color] * filter[N] \
                + src_buf[2 * width + stream - 2 * color] * filter[NE] \
                + Nline[width + stream - color] * filter[S] \
                + Nline[width + stream - 2 * color] * filter[SE] \
                + Ecolumn[stream] * filter[W] \
                + Ecolumn[stream + color] * filter[NW] \
                + NEc[stream] * filter[SW]);

            // SW corner
            dest_buf[(height - 1) * width + stream] = ( \
                src_buf[(height - 1) * width + stream] * filter[C] \
                + src_buf[(height - 1) * width + stream + color] * filter[W] \
                + src_buf[(height - 1) * width + stream - width] * filter[S] \
                + src_buf[(height - 2) * width + stream + color] * filter[SW] \
                + Sline[stream] * filter[N] \
                + Sline[stream + color] * filter[NW] \
                + Wcolumn[height + stream - color] * filter[E] \
                + Wcolumn[height + stream - 2 * color] * filter[SE]  \
                + SWc[stream] * filter[NE]);

            // SE corner
            dest_buf[height * width + stream - color] = ( \
                src_buf[height * width + stream - color] * filter[C] \
                + src_buf[height * width + stream - 2 * color] * filter[E] \
                + src_buf[height * width + stream - color - width] * filter[S] \
                + src_buf[height * width + stream - width - 2 * color] * filter[SE] \
                + Sline[width + stream - color] * filter[N] \
                + Sline[width + stream - 2 * color] * filter[NE] \
                + Ecolumn[height + stream - color] * filter[W] \
                + Ecolumn[height + stream - 2 * color] * filter[SW] \
                + SEc[stream] * filter[NW]);
        }

        // wait for all sends to process
        MPI_Waitall(8, sendRequests, MPI_STATUSES_IGNORE);

        // swap src and dest buffers for the next round
        tmp = dest_buf;
        dest_buf = src_buf;
        src_buf = tmp;

        // every REDUCEROUNDS rounds perform MPI_Reduce
        // REDUCEROUNDS is defined at hybrid_utils.h
        if ((step % REDUCEROUNDS) == REDUCEROUNDS - 1) {

            unsigned char global_diff, local_diff = 0;

            // find if last round made any difference
            // send 1 if it did, or 0 if it didn't
            for (int i = 0; i < width * height; i++)
                if (src_buf[i] != dest_buf[i]) {
                    local_diff = 1;
                    break;
                }

            // use predefined reduction operator (Logical OR)
            // to check if there is any difference,
            // in any local buffer, at the last round
            MPI_Allreduce(&local_diff, &global_diff, 1,
                MPI_UNSIGNED_CHAR, MPI_LOR, cartesian);

            // if the Logical OR operation is 0,
            // then all processes sent 0, so the image has converged.
            // but we won't stop either way.
            // in case we would like to stop, we can do:
            // if (global_diff == 0)
            //     break;
        }
    }

    // wait until the slowest process
    MPI_Barrier(cartesian);

    // calculate the elapsed time
    timer = MPI_Wtime() - timer;

    // in case you are the master process,
    // print elapsed time (the time of the slowest process)
    if (rank == 0)
        printf("Elapsed time: %.3f sec.\n", timer);

    // free defined custom datatype for pixel, line and column
    MPI_Type_free(&column);
    MPI_Type_free(&line);
    MPI_Type_free(&pixel);

    // free extra local arrays for neighbours' borders
    free(Nline);
    free(Sline);
    free(Wcolumn);
    free(Ecolumn);

    return src_buf;
}
