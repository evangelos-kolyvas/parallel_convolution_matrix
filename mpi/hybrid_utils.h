/**********************************************************************************
 * Project              : Parallel Computer Systems
 * Author               : Kolyvas Evangelos - M1595 - evangelos.kolyvas@gmail.com
 * Date                 : January 31, 2018
 * Description          : Convolution Matrix - hybrid_utils.h
 * --------------------------------------------------------------------------------
 * Department of IT, National and Kapodistrian University of Athens.
 **********************************************************************************/

#include <mpi.h>            // MPI functions
#include <stdlib.h>         // atoi, malloc, free
#include <stdio.h>          // printf, fopen, fclose, fread, fwrite
#include <math.h>           // sqrt

#define REDUCEROUNDS 10     // we use this to measure reduce impact in our program
                            // it will not affect iterations

#ifdef _OPENMP              // conditional compilation: take care in case
#include <omp.h>            // the compiler doesn't support OpenMP
#define THREADSPERCORE 2    // in case of OpenMP, maximum 2 threads per core.
#endif                      // more threads per core will be inefficient

/**********************************************************************************
 * I don't want to call functions inside my main loop.
 * Functions will slow down my program.
 * Program counter and all local variables have to store their values to the stack,
 * and then jump to the new code segment.
 * So I made some macros to help things with repetitive code segments.
 * Macros will just expand the source code at pre-compilation time.
 **********************************************************************************/

// shortcut for adding the 3 pixels in the previous row from OFFSET,
// multiplied by their coefficients. if STREAMCOUNT is 1 then we add the 3 bytes
// directly above the byte at OFFSET (greyscale) otherwise we add every 3rd byte (RGB).
#define ADD_UPPER_ROW(OFFSET, STREAMCOUNT) (src_buf[OFFSET-width-STREAMCOUNT] * filter[SE] + \
    src_buf[OFFSET-width] * filter[S] + src_buf[OFFSET-width+STREAMCOUNT] * filter[SW])

// add the pixels at OFFSET, before OFFSET and after OFFSET
#define ADD_MIDDLE_ROW(OFFSET, STREAMCOUNT) (src_buf[OFFSET] * filter[C] + \
    src_buf[OFFSET-STREAMCOUNT] * filter[E] + src_buf[OFFSET+STREAMCOUNT] * filter[W])

// add the pixels in the row below OFFSET
#define ADD_DOWN_ROW(OFFSET, STREAMCOUNT) (src_buf[OFFSET+width-STREAMCOUNT] * filter[NE] + \
    src_buf[OFFSET+width] * filter[N] + src_buf[OFFSET+width+STREAMCOUNT] * filter[NW])

// add the pixels at the column left of OFFSET
#define ADD_LEFT_COL(OFFSET, STREAMCOUNT) (src_buf[OFFSET-width-STREAMCOUNT] * filter[SE] + \
    src_buf[OFFSET-1] * filter[E] + src_buf[OFFSET+width-STREAMCOUNT] * filter[NE])

// add the pixels at OFFSET, above it and below it
#define ADD_MIDDLE_COL(OFFSET, STREAMCOUNT) (src_buf[OFFSET] * filter[C] + \
    src_buf[OFFSET-width] * filter[S] + src_buf[OFFSET+width] * filter[N])

// add the pixels at the column left of OFFSET
#define ADD_RIGHT_COL(OFFSET, STREAMCOUNT) (src_buf[OFFSET-width+STREAMCOUNT] * filter[SW] + \
    src_buf[OFFSET+STREAMCOUNT] * filter[W] + src_buf[OFFSET+width+STREAMCOUNT] * filter[NW])

// colors:
// GREY (1 byte  data)
// RGB  (3 bytes data)
typedef enum {GREY = 1, RGB = 3} color_t;

// directions:
// N = North
// S = South
// W = West
// E = East
// C = Center
typedef enum {NW, N, NE, W, C, E, SW, S, SE} direction;

// core function, image convolution, declaration
unsigned char *convolution(int rank, const int width, const int height,
    const color_t color, const unsigned int total_steps,
    MPI_Comm cartesian, unsigned char *src_buf, unsigned char *dest_buf);
